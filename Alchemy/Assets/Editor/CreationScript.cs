﻿using UnityEngine;
using UnityEditor;

public class CreationScript : MonoBehaviour
{
    [MenuItem("Tool Creation/Create Folder")]

    public static void CreateFolder()
    {
        AssetDatabase.CreateFolder("Assets", "Materials");
        AssetDatabase.CreateFolder("Assets", "Textures");
        AssetDatabase.CreateFolder("Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Animations", "AnimationControllers");

        System.IO.File.WriteAllText(Application.dataPath +
            "/Materials/folderStructure.txt", "This folder is for storing Materials!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Textures/folderStructure.txt", "This folder is for storing Textures!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Prefabs/folderStructure.txt", "This folder is for storing Prefabs!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Scripts/folderStructure.txt", "This folder is for storing Scripts!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Scenes/folderStructure.txt", "This folder is for storing Scenes!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Animations/folderStructure.txt", "This folder is for storing raw Animations!");
        System.IO.File.WriteAllText(Application.dataPath +
            "/Animations/AnimationControllers/folderStructure.txt", "This folder is for storing Animation Controllers!");

        AssetDatabase.Refresh();
    }
}
