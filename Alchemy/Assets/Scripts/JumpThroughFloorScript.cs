﻿using UnityEngine;
using System.Collections;

public class JumpThroughFloorScript : MonoBehaviour
{
    public GameObject Floor;

    void OnCollisionStay(Collision player)
    {
        if(player.gameObject.tag == "Player")
        {
            if(Input.GetKeyDown(KeyCode.S))
            {
                Floor.GetComponent(BoxCollider).enabled = false;
            }
        }
    }

    void OnCollisionExit(Collision player)
    {
        if(player.gameObject.tag=="Player")
        {
            Floor.GetComponent(BoxCollider).enabled = true;
        }
    }
}
