﻿using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    public GameObject Player;
    public int jumpHeight;
    public float movementSpeed;

    private float lockPos = 0;
    

    // Use this for initialization
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lockPos, lockPos);

        if (Input.GetKeyDown(KeyCode.S))
        {
            transform.localScale -= new Vector3(0, 1f, 0);
            if(Physics.Raycast(transform.position, Vector3.up, 2))
            {
                Debug.Log("hit down");
            }
            
        }

        if (Input.GetKeyUp(KeyCode.S))
        {

            do
            {
                Debug.Log("hit up");
                //transform.localScale -= new Vector3(0, 0f, 0);
            } while (Physics.Raycast(transform.position, Vector3.up, 2));
          //  else
            //{
               transform.localScale += new Vector3(0, 1f, 0);
            //}
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(movementSpeed, 0, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-movementSpeed, 0, 0);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            transform.Translate(0, jumpHeight, 0);
        }
        
        //if(Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.A))
        //{
          //transform.localScale += new Vector3(1f, 0, 0);
        //}

    }
}
