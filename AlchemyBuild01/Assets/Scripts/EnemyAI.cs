﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
    public float moveSpeed;
    public float satisfaction;
    public float arrival;
    public Transform currentTarget;
    public PlayerStatsScript playerHealth;
    int attackCooldown = 0;

    void Update()
    {
        EnemyArrive(currentTarget, moveSpeed, arrival, satisfaction);
        playerHealth = GameObject.Find("Player").GetComponent<PlayerStatsScript>();
    }
	public void EnemyArrive(Transform target, float speed, float arriveTime, float satisRad)
    {
        //Get a vector from the current position to the target position
        Vector3 moveDirection = target.position - transform.position;
        //Check to see if the enemy is already at the target
        if (moveDirection.magnitude < satisRad)
            return;
        //If enemy if not at target, calc how long it will take to arrive
        float calcSpeed = moveDirection.magnitude / arriveTime;
        //Enemy cannot move too fast, so set the calc speed to the max
        if (calcSpeed > speed)
            calcSpeed = speed;
        //Gives moveDirection a value of 1
        moveDirection.Normalize();

        //Calculate how far to move in the current frame
        moveDirection = moveDirection * speed * Time.deltaTime;

        //Do the movement
        transform.Translate(moveDirection, Space.World);
        EnemyAttack();
    }
    public void EnemyAttack()
    {
        attackCooldown++;
        if (attackCooldown >= 10)
        {
            playerHealth.currentHP -= 10;
            attackCooldown = 0;
        }
    }
    //void OnTriggerStay2D(Collider2D col)
    //{
    //    Debug.Log("Attack");
    //    EnemyAttack();
    //}
}
