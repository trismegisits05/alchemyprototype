﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode()]
public class Crosshair : MonoBehaviour {

    public Texture2D crosshair;
    public GameObject crosshairPos;
    public float xPos;
    public float yPos;

	// Use this for initialization
	void Start () {
        xPos = (Screen.width / 2) - (crosshair.width / 2);
        yPos = (Screen.width / 2) - (crosshair.width / 2);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey("right"))
        {
            xPos = (Screen.width / 2) + 100;
            yPos = (Screen.height / 2) - (crosshair.height / 2);
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if(Input.GetKey("up"))
        {
            xPos = (Screen.width / 2) - (crosshair.width / 2);
            yPos = (Screen.height / 2) - 150;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if (Input.GetKey("down"))
        {
            xPos = (Screen.width / 2) - (crosshair.width / 2);
            yPos = (Screen.height / 2) + 100;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if (Input.GetKey("left"))
        {
            xPos = (Screen.width / 2) - 150;
            yPos = (Screen.height / 2) - (crosshair.height / 2);
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if(Input.GetKey("right") && Input.GetKey("up"))
        {
            xPos = (Screen.width / 2) + 100;
            yPos = (Screen.height / 2) - 150;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if(Input.GetKey("right") && Input.GetKey("down"))
        {
            xPos = (Screen.width / 2) + 100;
            yPos = (Screen.height / 2) + 100;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if(Input.GetKey("left") && Input.GetKey("down"))
        {
            xPos = (Screen.width / 2) - 150;
            yPos = (Screen.height / 2) + 100;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
        if(Input.GetKey("left") && Input.GetKey("up"))
        {
            xPos = (Screen.width / 2) - 150;
            yPos = (Screen.height / 2) - 150;
            crosshairPos.transform.position = new Vector2(xPos, yPos);
        }
	}
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(xPos, yPos, crosshair.width, crosshair.height), crosshair);
    }
}
