﻿using UnityEngine;
using System.Collections;

public class UpgradeScript : MonoBehaviour
{
    public GameObject typeOfUpgrade;
    public int hpUpgrade;

    [HideInInspector]
    public PlayerStatsScript playerStats;

    void OnCollisionEnter(Collision player)
    {
        if(player.gameObject.tag == "Player")
        {
            playerStats = player.gameObject.GetComponent<PlayerStatsScript>();
            if(typeOfUpgrade.tag == "HealthUpgrade")
            {
                playerStats.baseHP += hpUpgrade;
            }
            Destroy(gameObject);
        }
    }
}
