﻿using UnityEngine;
using System.Collections;

public class Collisions : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Tree")
        {
            Destroy(col.gameObject);
        }
        else if(col.gameObject.tag == "bush")
        {
            Destroy(col.gameObject);
        }
        else if(col.gameObject.tag == "vines")
        {
            Destroy(col.gameObject);
        }
    }
    
}
