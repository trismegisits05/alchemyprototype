﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Manipulators : MonoBehaviour {

    public Rigidbody2D heatProj;
    public Rigidbody2D chakProj;

    public bool heatEquipped = false;
    public bool noManipEquipped = false;

    public Image heatImage;
    public Transform targetLocation;

    public float projectileSpeed;

    
    int currentManipulator;
	// Use this for initialization
	void Start ()
    {
        heatProj = heatProj.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("f"))
        {
            if (noManipEquipped)
            {
                Destroy(GameObject.FindWithTag("Projectile"));
                currentManipulator = 0;
                Chakram(targetLocation, projectileSpeed);
            }
            else if (heatEquipped)
            {
                Destroy(GameObject.FindWithTag("Projectile"));
                currentManipulator = 1;
                Heat(targetLocation, projectileSpeed);
            }
        }
        if(Input.GetKeyDown("q"))
        {
            if (currentManipulator == 1)
            {
                currentManipulator = 4;
                heatImage.color = Color.red;
                heatEquipped = true;
            }
            else if (currentManipulator == 2)
            {
                currentManipulator = 1;
                heatImage.color = Color.green;
            }
            else if (currentManipulator == 3)
            {
                currentManipulator = 2;
                heatImage.color = Color.red;
            }
            else if (currentManipulator == 4)
            {
                currentManipulator = 3;
                heatImage.color = Color.red;
            }
            
        }
        if (Input.GetKeyDown("e"))
        {
            if(currentManipulator == 1)
            {
                currentManipulator = 2;
                heatImage.color = Color.red;
            }
            else if(currentManipulator == 2)
            {
                currentManipulator = 3;
                heatImage.color = Color.red;
            }
            else if(currentManipulator == 3)
            {
                currentManipulator = 4;
                heatImage.color = Color.red;
            }
            else if(currentManipulator == 4)
            {
                currentManipulator = 1;
                heatImage.color = Color.green;
            }
        }
	}
    void Heat(Transform target, float speed)
    {
        Rigidbody2D clone;
        clone = Instantiate(heatProj, transform.position, transform.rotation) as Rigidbody2D;
        clone.tag = "Projectile";
        if (target.transform.position.x > 500 && target.transform.position.y > 200)
        {
            clone.AddForce(Vector2.right * speed);
        }
        else if(target.transform.position.x == 543 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.up * speed);
        }
        else if(target.transform.position.x == 543 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.down * speed);
        }
        else if(target.transform.position.x == 424 && target.transform.position.y == 228)
        {
            clone.AddForce(Vector2.left * speed);
        }
        else if(target.transform.position.x == 674 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.right * speed);
            clone.AddForce(Vector2.up * speed);
        }
        else if(target.transform.position.x == 674 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.right * speed);
            clone.AddForce(Vector2.down * speed);
        }
        else if(target.transform.position.x == 424 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.left * speed);
            clone.AddForce(Vector2.down * speed);
        }
        else if(target.transform.position.x == 424 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.left * speed);
            clone.AddForce(Vector2.up * speed);
        }
        else
        {
            Debug.Log("Invalid direction");
        }
        Destroy(GameObject.FindWithTag("Projectile"), 0.12f);
    }    

    void Chakram(Transform target, float speed)
    {
        Rigidbody2D clone;
        clone = Instantiate(chakProj, transform.position, transform.rotation) as Rigidbody2D;
        clone.tag = "Projectile";

        if (target.transform.position.x > 500 && target.transform.position.y > 200)
        {
            clone.AddForce(Vector2.right * speed);
        }
        else if (target.transform.position.x == 543 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.up * speed);
        }
        else if (target.transform.position.x == 543 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.down * speed);
        }
        else if (target.transform.position.x == 424 && target.transform.position.y == 228)
        {
            clone.AddForce(Vector2.left * speed);
        }
        else if (target.transform.position.x == 674 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.right * speed);
            clone.AddForce(Vector2.up * speed);
        }
        else if (target.transform.position.x == 674 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.right * speed);
            clone.AddForce(Vector2.down * speed);
        }
        else if (target.transform.position.x == 424 && target.transform.position.y == 360)
        {
            clone.AddForce(Vector2.left * speed);
            clone.AddForce(Vector2.down * speed);
        }
        else if (target.transform.position.x == 424 && target.transform.position.y == 110)
        {
            clone.AddForce(Vector2.left * speed);
            clone.AddForce(Vector2.up * speed);
        }
    }
    
    
}
