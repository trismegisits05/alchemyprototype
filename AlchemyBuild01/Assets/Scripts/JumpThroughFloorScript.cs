﻿using UnityEngine;
using System.Collections;

public class JumpThroughFloorScript : MonoBehaviour
{
    public GameObject Floor;

    void OnCollisionStay(Collision player)
    {
        if (player.gameObject.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                Floor.GetComponent<BoxCollider>().isTrigger = true;
            }
        }
    }

    void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == "Player")
        {            
           Floor.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}